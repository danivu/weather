


var Weather = {

    getWeather: function (location) {
        $.ajax({
            url: "/weather/check/" + location,
            type: "GET",
            statusCode: {
                422: function () {
                   $('#forecastsList').html('Brak danych pogodowych');
                }
            }
        }).done(function (data) {
                    $('#location_city').html(data.locationCity);
                    $('#location_country').html(data.locationCountry);
                    $('#condition_temperature').html(data.conditionTemperature + '&#x2103;');
                    $('#condition_text').html(data.conditionText);
                    
                    $('#forecastsList').html('');
                    $.each(data.forecasts, function (key, value) {
                        var html = "<td >" + value.day + "</td><td>" + value.low + "</td><td>" + value.high + "</td><td>" + value.text + "</td>";

                        $('#forecastsList').append('<tr>' + html + '</tr>');
                    });


                });
    },

    updateLocationList: function () {
        var that = this;

        $.ajax({
            url: "/weather/locations/"
        }).done(function (data) {
            $.each(data, function (key, value) {
                $('#location').append('<option value="' + value + '">' + value + '</option>');
            });
            if (Object.keys(data)[0].length > 0) {
                that.getWeather(Object.keys(data)[0]);
            }
        });
    }
}

$(document).ready(function () {


    Weather.updateLocationList();


    $('#location').change(function () {
        Weather.getWeather($(this).val());
    });



});

