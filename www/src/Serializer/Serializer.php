<?php

namespace App\Serializer;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer as SymfonySerializer;
use App\Entity\Weather;

/**
 * Description of WeatherSerializer
 *
 * @author Daniel
 */
class Serializer
{

    private function getSerializer()
    {
        $encoders   = [new XmlEncoder(), new JsonEncoder()];
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        return new SymfonySerializer([$normalizer], $encoders);
    }

    public function serializeToJson( $entity)
    {
        $serializer = $this->getSerializer();
        return $serializer->serialize($entity, 'json');
    }
}