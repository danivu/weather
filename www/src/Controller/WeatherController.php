<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Weather\WeatherContext;
use App\Weather\Yahoo;
use App\Entity\Locations;
use App\Entity\ApiConfig;

use App\Serializer\Serializer;


/**
 * Description of WeatherController
 *
 * @author Daniel
 */
class WeatherController extends AbstractController
{

    public function check(string $location)
    {
        $apiConfig = $this->getDoctrine()
            ->getRepository(ApiConfig::class)
            ->findLast();
        $yahoo     = new Yahoo($apiConfig);

        $weather = new WeatherContext($yahoo, $this->getDoctrine()->getManager());
        $entity    = $weather->findWeather($location);

        if (!empty($entity)) {
            $json     = (new Serializer())->serializeToJson($entity);
            $response = new Response(
                $json, Response::HTTP_OK, ['content-type' => 'application/json']
            );

            return $response;
        }

        $response = new Response(
            "", Response::HTTP_UNPROCESSABLE_ENTITY, ['content-type' => 'application/json']
        );


        return $response;

    }

    public function locationsList()
    {

        $locations = $this->getDoctrine()
            ->getRepository(Locations::class)
            ->findAll();

        $list = [];
        foreach ($locations as $location) {
            $fullString        = "{$location->getCity()},{$location->getCountry()}";
            $list[$fullString] = $fullString;
        }

        $response = new Response(
            json_encode(
                $list
            ), Response::HTTP_OK, ['content-type' => 'application/json']
        );

        return $response;
    }
}