<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Description of HomeController
 *
 * @author Daniel
 */
class HomeController extends AbstractController
{

    public function index()
    {
        return $this->render('homepage.html.twig');
    }
}