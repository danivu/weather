<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error        = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('@EasyAdmin/page/login.html.twig',
                [
                // parameters usually defined in Symfony login forms
                'error'                => $error,
                'last_username'        => $lastUsername,
                'translation_domain'   => 'admin',
                'page_title'           => 'Administracja',
                'csrf_token_intention' => 'authenticate',
                'target_path'          => $this->generateUrl('admin_dashboard'),
                'username_label'       => 'Login',
                'password_label'       => 'Hasło',
                'sign_in_label'        => 'Log in',
                'username_parameter'   => '_username',
                'password_parameter'   => '_password',
        ]);
    }

 
}