<?php

namespace App\Controller\Admin;

use App\Entity\ApiConfig;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;


class ApiConfigCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ApiConfig::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            UrlField::new('url'),
            Field::new('consumerKey'),
            Field::new('consumerSecret')
        ];
    }
    
}
