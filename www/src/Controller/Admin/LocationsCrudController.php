<?php

namespace App\Controller\Admin;

use App\Entity\Locations;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

class LocationsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Locations::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            Field::new('city','Miasto'),
            Field::new('country','Kraj'),
        ];
    }
    
}
