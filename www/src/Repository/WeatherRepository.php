<?php

namespace App\Repository;

use App\Entity\Weather;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Weather|null find($id, $lockMode = null, $lockVersion = null)
 * @method Weather|null findOneBy(array $criteria, array $orderBy = null)
 * @method Weather[]    findAll()
 * @method Weather[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeatherRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Weather::class);
    }


    public function getLastByLocation($location)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select(array('w'))
            ->from(Weather::class, 'w')
            ->innerJoin(\App\Entity\ForecastsWeather::class, 'f', 'WITH', 'f.weather = w.id')
            ->where('w.locationName = :location')
            ->setParameter('location', $location)
            ->orderBy('w.id', 'DESC');


        $query   = $qb->getQuery();
        $results = $query->getResult();
        if (!empty($results[0])) {
            return $results[0];
        }
        return [];
    }
    // /**
    //  * @return Weather[] Returns an array of Weather objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Weather
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
