<?php

namespace App\Repository;

use App\Entity\ForecastsWeather;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ForecastsWeather|null find($id, $lockMode = null, $lockVersion = null)
 * @method ForecastsWeather|null findOneBy(array $criteria, array $orderBy = null)
 * @method ForecastsWeather[]    findAll()
 * @method ForecastsWeather[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ForecastsWeatherRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ForecastsWeather::class);
    }

    // /**
    //  * @return ForecastsWeather[] Returns an array of ForecastsWeather objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ForecastsWeather
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
