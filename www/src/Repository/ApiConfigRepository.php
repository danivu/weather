<?php

namespace App\Repository;

use App\Entity\ApiConfig;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ApiConfig|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApiConfig|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApiConfig[]    findAll()
 * @method ApiConfig[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApiConfigRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ApiConfig::class);
    }


    public function findLast()
    {
        $result = $this->createQueryBuilder('a')
            ->orderBy('a.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
        ;

        if(empty($result)){
            return [];
        }
        return $result[0];
    }


    // /**
    //  * @return ApiConfig[] Returns an array of ApiConfig objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ApiConfig
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
