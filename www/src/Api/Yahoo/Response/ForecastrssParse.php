<?php

namespace App\Api\Yahoo\Response;

/**
 * Description of YahooParser
 *
 * @author Daniel
 */
class ForecastrssParse
{
    private $obj;

    public function __construct($obj)
    {
        $this->obj = $obj;
    }

    private function toForecastsWeatherEntity($entityWeather)
    {

        $forecasts = $this->obj->forecasts;

        if (!is_array($forecasts)) {
            throw new \Exception("forecasts must be Array");
        }

        $result = [];

        foreach ($forecasts as $forecast) {
            $entity = new \App\Entity\ForecastsWeather();
            $date   = (new \DateTime())->setTimestamp($forecast->date);
            $entity->setDate($date)
                ->setDay($forecast->day)
                ->setLow($forecast->low)
                ->setHigh($forecast->high)
                ->setText($forecast->text)
                ->setCode($forecast->code)
                ->setWeather($entityWeather);

            $result[] = $entity;
        }

        return $result;
    }

    public function getWeatherEntity()
    {

        $location   = $this->obj->location;
        $astronomy  = $this->obj->current_observation->astronomy;
        $atmosphere = $this->obj->current_observation->atmosphere;
        $condition  = $this->obj->current_observation->condition;
        $wind       = $this->obj->current_observation->wind;

        $pubDate = (new \DateTime())->setTimestamp($this->obj->current_observation->pubDate);

        $entity = (new \App\Entity\Weather())
            ->setLocationCity($location->city)
            //todo ->setLocationName($location->city)
            ->setLocationCountry($location->country)
            ->setLocationLat($location->lat)
            ->setLocationLong($location->long)
            ->setLocationRegion($location->region)
            ->setLocationTimezone($location->timezone_id)
            ->setAstronomySunrise($astronomy->sunrise)
            ->setAstronomySunset($astronomy->sunset)
            ->setAtmosphereHumidity($atmosphere->humidity)
            ->setAtmospherePressure($atmosphere->pressure)
            ->setAtmosphereRising($atmosphere->rising)
            ->setAtmosphereVisibility($atmosphere->visibility)
            ->setConditionCode($condition->code)
            ->setConditionTemperature($condition->temperature)
            ->setConditionText($condition->text)
            ->setWindChill($wind->chill)
            ->setWindDirection($wind->direction)
            ->setWindSpeed($wind->speed)
            ->setPubDate($pubDate);

        $forecasts = $this->toForecastsWeatherEntity($entity);
        $entity->setForecasts($forecasts);

        return $entity;
    }
}