<?php

namespace App\Api\Yahoo\Request;

/**
 * Description of ForecastrssRequest
 *
 * @author Daniel
 */
class ForecastrssRequest implements RequestInterface
{
    private $method = "GET";
    private $format = "json";
    private $units  = "c";
    private $location;

    /**
     * @param string $location
     * @return \self
     */
    public function setLocation($location): self
    {
        $this->location = $location;
        return $this;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return "forecastrss";
    }

    /**
     * @return array
     */
    public function getParams() : array
    {
        return [
            'query' => [
                'location' => $this->location,
                'format'   => $this->format,
                'u'        => $this->units
            ]
        ];
    }
}