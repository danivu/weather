<?php

namespace App\Api\Yahoo\Request;

/**
 * Description of RequestInterface
 *
 * @author Daniel
 */
interface RequestInterface
{

    public function getMethod() : string;

    public function getUrl() : string;

    public function getParams() : array;
}