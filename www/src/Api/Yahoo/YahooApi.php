<?php

namespace App\Api\Yahoo;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;

/**
 * Description of YahooApi
 *
 * @author Daniel
 */
class YahooApi
{
    private $consumerKey;
    private $consumerSecret;
    private $apiUrl;
    private $errorMessage;
    private $response;

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    public function setApiUrl(string $apiUrl): self
    {
        $this->apiUrl = $apiUrl;
        return $this;
    }

    public function setConsumerKey(string $consumerKey): self
    {
        $this->consumerKey = $consumerKey;
        return $this;
    }

    public function setConsumerSecret(string $consumerSecret): self
    {
        $this->consumerSecret = $consumerSecret;
        return $this;
    }

    /**
     *
     * @return Client
     */
    private function client()
    {
        $handlerStack = HandlerStack::create();

        $middleware = new Oauth1([
            'consumer_key'    => $this->consumerKey,
            'consumer_secret' => $this->consumerSecret,
            'token'           => '',
            'token_secret'    => ''
        ]);
        $handlerStack->push($middleware);

        $client = new Client([
            'base_uri' => $this->apiUrl,
            'handler'  => $handlerStack,
            'auth'     => 'oauth'
        ]);

        return $client;
    }

    public function request(Request\RequestInterface $request)
    {
        try {
            $getResponse    = $this->client()->request(
                $request->getMethod(), $request->getUrl(), $request->getParams()
            );
            $this->response = $getResponse->getBody()->getContents();
            return true;
        } catch (ConnectException $exception) {
            $this->errorMessage = $exception->getMessage();
        } catch (ClientException $exception) {
            $this->errorMessage = $exception->getMessage();
        }

        return false;
    }

    public function getResponse()
    {
        return $this->response;
    }
}