<?php

namespace App\Weather;

use App\Entity\ApiConfig;
use App\Api\Yahoo\YahooApi;
use App\Api\Yahoo\Request\ForecastrssRequest;
use App\Api\Yahoo\Response\ForecastrssParse;

/**
 * Description of Yahoo
 *
 * @author Daniel
 */
class Yahoo implements SourceInterface
{
    /**
     * @var ApiConfig
     */
    private $config;

    public function __construct(ApiConfig $apiConfig)
    {
        $this->config = $apiConfig;
    }

    private function createApi()
    {
        $api = new YahooApi();
        $api->setApiUrl($this->config->getUrl())
            ->setConsumerKey($this->config->getConsumerKey())
            ->setConsumerSecret($this->config->getConsumerSecret());

        return $api;
    }

    /**
     *
     * @param type $location
     * @return App\Entity\Weather
     */
    public function getWeatherByLocation($location)
    {
        $forecastsRequest = new ForecastrssRequest();
        $forecastsRequest->setLocation($location);
        $api = $this->createApi();
        $response = $api->request($forecastsRequest);
        if (!empty($response)) {
            $obj = \json_decode($api->getResponse());
            $parser        = new ForecastrssParse($obj);
            $weatherEntity = $parser->getWeatherEntity();
            $weatherEntity->setLocationName($location);
            return $weatherEntity;
        }
        return false;
    }
}