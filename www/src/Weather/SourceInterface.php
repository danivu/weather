<?php

namespace App\Weather;

use App\Entity\Weather;

/**
 * Description of WeatherInterface
 *
 * @author Daniel
 */
interface SourceInterface
{

    public function getWeatherByLocation($location);
}