<?php

namespace App\Weather;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Weather;

/**
 * Description of Weather
 *
 * @author Daniel
 */
class WeatherContext
{
    private $source;
    private $em;

    public function __construct(SourceInterface $source, EntityManagerInterface $em)
    {
        $this->source = $source;
        $this->em     = $em;
    }

    public function findWeather($location)
    {
        $wEntity = $this->source->getWeatherByLocation($location);
        if ($wEntity instanceof Weather) {
            $this->em->persist($wEntity);
            $this->em->flush();
            
        } else {
            $wEntity = $this->em
                ->getRepository(Weather::class)
                ->getLastByLocation($location);
        }

        return $wEntity;
    }
}