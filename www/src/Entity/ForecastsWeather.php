<?php

namespace App\Entity;

use App\Repository\ForecastsWeatherRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ForecastsWeatherRepository::class)
 */
class ForecastsWeather
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $day;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $low;

    /**
     * @ORM\Column(type="integer")
     */
    private $high;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $text;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $code;


    /**
     * @ORM\ManyToOne(targetEntity="Weather",inversedBy="forecasts")
     * @ORM\JoinColumn(name="weather_id", referencedColumnName="id")
     */
    private $weather;
    
    public function getWeather()
    {
        return $this->weather;
    }

    public function setWeather($weather): void
    {
        $this->weather = $weather;
    }

    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDay(): ?string
    {
        return $this->day;
    }

    public function setDay(string $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getLow(): ?int
    {
        return $this->low;
    }

    public function setLow(int $low): self
    {
        $this->low = $low;

        return $this;
    }

    public function getHigh(): ?int
    {
        return $this->high;
    }

    public function setHigh(int $high): self
    {
        $this->high = $high;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getCode(): ?int
    {
        return $this->code;
    }

    public function setCode(?int $code): self
    {
        $this->code = $code;

        return $this;
    }
}
