<?php

namespace App\Entity;

use App\Repository\WeatherRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=WeatherRepository::class)
 */
class Weather
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $locationName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $locationCity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $locationCountry;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $locationLat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $locationLong;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $locationRegion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $locationTimezone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $astronomySunrise;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $astronomySunset;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $atmosphereHumidity;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $atmospherePressure;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $atmosphereRising;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $atmosphereVisibility;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $conditionCode;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $conditionTemperature;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $conditionText;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $windChill;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $windDirection;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $windSpeed;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $pubDate;

    /**
     * @ORM\OneToMany(targetEntity="ForecastsWeather", mappedBy="weather",cascade={"persist"})
     */
    private $forecasts;


    public function __construct()
    {
        $this->forecasts = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocationName(): ?string
    {
        return $this->locationName;
    }

    public function setLocationName(string $locationName): self
    {
        $this->locationName = $locationName;

        return $this;
    }

    public function getLocationCity(): ?string
    {
        return $this->locationCity;
    }

    public function setLocationCity(?string $locationCity): self
    {
        $this->locationCity = $locationCity;

        return $this;
    }

    public function getLocationCountry(): ?string
    {
        return $this->locationCountry;
    }

    public function setLocationCountry(?string $locationCountry): self
    {
        $this->locationCountry = $locationCountry;

        return $this;
    }

    public function getLocationLat(): ?float
    {
        return $this->locationLat;
    }

    public function setLocationLat(?float $locationLat): self
    {
        $this->locationLat = $locationLat;

        return $this;
    }

    public function getLocationLong(): ?float
    {
        return $this->locationLong;
    }

    public function setLocationLong(?float $locationLong): self
    {
        $this->locationLong = $locationLong;

        return $this;
    }

    public function getLocationRegion(): ?string
    {
        return $this->locationRegion;
    }

    public function setLocationRegion(?string $locationRegion): self
    {
        $this->locationRegion = $locationRegion;

        return $this;
    }

    public function getLocationTimezone(): ?string
    {
        return $this->locationTimezone;
    }

    public function setLocationTimezone(?string $locationTimezone): self
    {
        $this->locationTimezone = $locationTimezone;

        return $this;
    }

    public function getAstronomySunrise(): ?string
    {
        return $this->astronomySunrise;
    }

    public function setAstronomySunrise(?string $astronomySunrise): self
    {
        $this->astronomySunrise = $astronomySunrise;

        return $this;
    }

    public function getAstronomySunset(): ?string
    {
        return $this->astronomySunset;
    }

    public function setAstronomySunset(?string $astronomySunset): self
    {
        $this->astronomySunset = $astronomySunset;

        return $this;
    }

    public function getAtmosphereHumidity(): ?int
    {
        return $this->atmosphereHumidity;
    }

    public function setAtmosphereHumidity(?int $atmosphereHumidity): self
    {
        $this->atmosphereHumidity = $atmosphereHumidity;

        return $this;
    }

    public function getAtmospherePressure(): ?int
    {
        return $this->atmospherePressure;
    }

    public function setAtmospherePressure(?int $atmospherePressure): self
    {
        $this->atmospherePressure = $atmospherePressure;

        return $this;
    }

    public function getAtmosphereRising(): ?int
    {
        return $this->atmosphereRising;
    }

    public function setAtmosphereRising(?int $atmosphereRising): self
    {
        $this->atmosphereRising = $atmosphereRising;

        return $this;
    }

    public function getAtmosphereVisibility(): ?float
    {
        return $this->atmosphereVisibility;
    }

    public function setAtmosphereVisibility(?float $atmosphereVisibility): self
    {
        $this->atmosphereVisibility = $atmosphereVisibility;

        return $this;
    }

    public function getConditionCode(): ?int
    {
        return $this->conditionCode;
    }

    public function setConditionCode(?int $conditionCode): self
    {
        $this->conditionCode = $conditionCode;

        return $this;
    }

    public function getConditionTemperature(): ?int
    {
        return $this->conditionTemperature;
    }

    public function setConditionTemperature(?int $conditionTemperature): self
    {
        $this->conditionTemperature = $conditionTemperature;

        return $this;
    }

    public function getConditionText(): ?string
    {
        return $this->conditionText;
    }

    public function setConditionText(?string $conditionText): self
    {
        $this->conditionText = $conditionText;

        return $this;
    }

    public function getWindChill(): ?int
    {
        return $this->windChill;
    }

    public function setWindChill(?int $windChill): self
    {
        $this->windChill = $windChill;

        return $this;
    }

    public function getWindDirection(): ?int
    {
        return $this->windDirection;
    }

    public function setWindDirection(?int $windDirection): self
    {
        $this->windDirection = $windDirection;

        return $this;
    }

    public function getWindSpeed(): ?int
    {
        return $this->windSpeed;
    }

    public function setWindSpeed(?int $windSpeed): self
    {
        $this->windSpeed = $windSpeed;

        return $this;
    }

    public function getPubDate(): ?\DateTimeInterface
    {
        return $this->pubDate;
    }

    public function setPubDate(?\DateTimeInterface $pubDate): self
    {
        $this->pubDate = $pubDate;

        return $this;
    }

    public function getForecasts()
    {
        return $this->forecasts;
    }

    public function setForecasts($forecasts): void
    {
        $this->forecasts = $forecasts;
    }


}