<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200701221355 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE weather (id INT AUTO_INCREMENT NOT NULL, location_name VARCHAR(255) NOT NULL, location_city VARCHAR(255) DEFAULT NULL, location_country VARCHAR(255) DEFAULT NULL, location_lat DOUBLE PRECISION DEFAULT NULL, location_long DOUBLE PRECISION DEFAULT NULL, location_region VARCHAR(255) DEFAULT NULL, location_timezone VARCHAR(255) DEFAULT NULL, astronomy_sunrise VARCHAR(255) DEFAULT NULL, astronomy_sunset VARCHAR(255) DEFAULT NULL, atmosphere_humidity INT DEFAULT NULL, atmosphere_pressure INT DEFAULT NULL, atmosphere_rising INT DEFAULT NULL, atmosphere_visibility DOUBLE PRECISION DEFAULT NULL, condition_code INT DEFAULT NULL, condition_temperature INT DEFAULT NULL, condition_text VARCHAR(255) DEFAULT NULL, wind_chill INT DEFAULT NULL, wind_direction INT DEFAULT NULL, wind_speed INT DEFAULT NULL, pub_date DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE weather');
    }
}
