<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200702195827 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE forecasts_weather CHANGE weather_id weather_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE forecasts_weather ADD CONSTRAINT FK_9E4F28CE675E FOREIGN KEY (weather_id) REFERENCES weather (id)');
        $this->addSql('CREATE INDEX IDX_9E4F28CE675E ON forecasts_weather (weather_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE forecasts_weather DROP FOREIGN KEY FK_9E4F28CE675E');
        $this->addSql('DROP INDEX IDX_9E4F28CE675E ON forecasts_weather');
        $this->addSql('ALTER TABLE forecasts_weather CHANGE weather_id weather_id INT NOT NULL');
    }
}
