<?php

namespace App\Tests\Util\App\Yahoo\Response;

use PHPUnit\Framework\TestCase;
use App\Api\Yahoo\Response\ForecastrssParse;

/**
 * Description of ResponseParseTest
 *
 * @author Daniel
 */
class ForecastrssParseTest extends TestCase
{
    protected $object;

    public function setUp()
    {

        $json = <<<JSON
            {"location":
            {"city":"Tychy","region":"Silesian","woeid":523318,"country":"Poland","lat":50.119469,"long":18.98443,"timezone_id":"Europe/Warsaw"},
            "current_observation":{"wind":{"chill":24,"direction":220,"speed":12.0},"atmosphere":{"humidity":63,"visibility":16.1,"pressure":982.0,
            "rising":0},"astronomy":{"sunrise":"4:43 am","sunset":"8:55 pm"},"condition":{"text":"Mostly Sunny","code":34,"temperature":24},
            "pubDate":1593972000},"forecasts":[{"day":"Sun","date":1593900000,"low":13,"high":27,"text":"Sunny","code":32},
            {"day":"Mon","date":1593986400,"low":18,"high":24,"text":"Partly Cloudy","code":30},{"day":"Tue","date":1594072800,"low":12,
            "high":17,"text":"Mostly Cloudy","code":28},{"day":"Wed","date":1594159200,"low":8,"high":21,"text":"Partly Cloudy","code":30}
            ,{"day":"Thu","date":1594245600,"low":15,"high":21,"text":"Mostly Cloudy","code":28},{"day":"Fri","date":1594332000,
            "low":13,"high":24,"text":"Thunderstorms","code":4},{"day":"Sat","date":1594418400,"low":15,"high":21,
            "text":"Scattered Thunderstorms","code":47},{"day":"Sun","date":1594504800,"low":12,"high":18,
            "text":"Partly Cloudy","code":30},{"day":"Mon","date":1594591200,"low":11,"high":18,"text":"Partly Cloudy",
            "code":30},{"day":"Tue","date":1594677600,"low":11,"high":20,"text":"Mostly Cloudy","code":28}]}
JSON;

        $obj = \json_decode($json);

        $this->object = new ForecastrssParse($obj);
    }

    public function testGetWeatherEntity()
    {
        $entity = $this->object->getWeatherEntity();;

        $entityArray = [
            $entity->getLocationCity(),
            $entity->getLocationCountry(),
            $entity->getLocationLat(),
            $entity->getLocationLong(),
            $entity->getLocationRegion(),
            $entity->getLocationTimezone(),
            $entity->getWindChill(),
            $entity->getWindDirection(),
            $entity->getWindSpeed(),
            $entity->getAstronomySunrise(),
            $entity->getAstronomySunset(),
            $entity->getAtmosphereHumidity(),
            $entity->getAtmospherePressure(),
            $entity->getAtmosphereVisibility(),
            $entity->getAtmosphereRising(),
            $entity->getConditionCode(),
            $entity->getConditionTemperature(),
            $entity->getConditionText()
        ];


        $testArray = [
            "Tychy",
            "Poland",
            50.119469,
            18.98443,
            "Silesian",
            "Europe/Warsaw",
            24,
            220,
            12,
            "4:43 am",
            "8:55 pm",
            63,
            982,
            16.1,
            0,
            34,
            24,
            "Mostly Sunny"
        ];




        $this->assertEquals($testArray, $entityArray);
    }
}